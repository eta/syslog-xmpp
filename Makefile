LISP ?= sbcl

all:
	$(LISP) \
		--disable-debugger \
		--eval '(ql:quickload :syslog-xmpp)' \
		--eval '(asdf:make :syslog-xmpp)' \
		--eval '(quit)'
