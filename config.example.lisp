(in-package :syslog-xmpp)

(defun determine-message-destinations (msg)
  "Returns a list of MUC addresses the given MSG (a SYSLOG-MESSAGE) should be sent to."
  (with-slots (hostname) msg
    (cond
      ((equal hostname "my-server") `("whatever@conference.place"))
      (t `("catchall@conference.place")))))

(defun user-main ()
  ;; Fill in your own values here
  (start "127.0.0.1" 541 "localhost" 5347 "component-password" "example-name.server.example"))
