(defsystem "syslog-xmpp"
  :depends-on ("sxcl" "usocket" "local-time" "split-sequence" "defclass-std")
  :serial t
  :build-operation "program-op"
  :build-pathname "syslog-xmpp"
  :entry-point "syslog-xmpp::main"
  :components
  ((:file "cursed")))
