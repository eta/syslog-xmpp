(defpackage :syslog-xmpp
  (:use :cl :defclass-std))

(in-package :syslog-xmpp)

(defun pri-facility-to-keyword (facility)
  "Returns a keyword representing the PRI facility number FACILITY."
  (ecase facility
    (0 :kernel)
    (1 :user)
    (2 :mail)
    (3 :system)
    (4 :security)
    (5 :syslog)
    (6 :lp)
    (7 :news)
    (8 :uucp)
    (9 :clock)
    (10 :security)
    (11 :ftp)
    (12 :ntp)
    (13 :audit)
    (14 :alert)
    (15 :clock2)
    (16 :local0)
    (17 :local1)
    (18 :local2)
    (19 :local3)
    (20 :local4)
    (21 :local5)
    (22 :local6)
    (23 :local7)))

(defun pri-severity-to-keyword (severity)
  "Returns a keyword representing the PRI severity number SEVERITY."
  (ecase severity
    (0 :emergency)
    (1 :alert)
    (2 :critical)
    (3 :error)
    (4 :warning)
    (5 :notice)
    (6 :info)
    (7 :debug)))

(defun pri-keyword-to-emoji (severity)
  "Returns an appropriate emoji for the PRI severity keyword SEVERITY."
  (ecase severity
    (:emergency "🔥")
    (:alert "🚨")
    (:critical "‼")
    (:error "❗")
    (:warning "⚠️")
    (:notice "🟠")
    (:info "🟢")
    (:debug "🔵")))

(defun parse-pri (pri)
  "Parses a syslog PRI value (e.g. <6>), returning the facility value and the severity value."
  (assert (> (length pri) 2) (pri) "Invalid PRI value ~A" pri)
  (let* ((value (parse-integer (subseq pri 1 (1- (length pri))))))
    (values (pri-facility-to-keyword (floor value 8))
            (pri-severity-to-keyword (mod value 8)))))

(defun parse-pri-version (pri-version)
  "Parses a syslog PRI-and-version value (e.g. <6>1), returning the facility value and the severity value, and signaling an error if the version is unsupported."
  (assert (> (length pri-version) 3) (pri-version) "Invalid PRI-and-version value ~A" pri-version)
  (assert (equal (elt pri-version (1- (length pri-version))) #\1) (pri-version) "Unsupported syslog version: ~A" pri-version)
  (parse-pri (subseq pri-version 0 (1- (length pri-version)))))

(defun parse-timestamp (timestamp)
  (local-time:parse-timestring timestamp))

(defmacro with-syslog-nullables ((&rest idents) &body body)
  `(let (,@(loop for ident in idents
                 collect `(,ident (unless (string= ,ident "-")
                                    ,ident))))
     ,@body))

(defclass/std syslog-message ()
  ((facility
    severity
    timestamp
    hostname
    app-name
    procid
    msgid
    msg)))

(defmethod print-object ((msg syslog-message) out)
  (print-unreadable-object (msg out :type t)
    (with-slots (facility severity timestamp hostname app-name procid msgid msg) msg
      (format out "<~A ~A> ~A ~A ~A[~A]: ~A" facility severity timestamp hostname app-name procid msg))))

(defun find-sd-right-bracket (input)
  "Find the last right bracket of the STRUCTURED-DATA in INPUT."
  (let ((ret))
    (if (or (eql (length input) 0)
            (eql (elt input 0) #\-))
        nil
        (loop
          do (setf ret (position #\] input
                                 :start (or (when ret (1+ ret)) 0)))
          while ret
          when (or (eql (1+ ret) (length input))
                   (eql (elt input (1+ ret)) #\Space))
            do (return-from find-sd-right-bracket ret)))))

(defun split-structured-data-and-msg (input)
  "Split INPUT, a STRUCTURED-DATA [SP MSG], into the STRUCTURED-DATA and MSG."
  (let ((last-right-bracket (find-sd-right-bracket input)))
    (if last-right-bracket
        ;; Is there a message after the structured data?
        (if (> (length input) (1+ last-right-bracket))
            (values (subseq input 0 (1+ last-right-bracket))
                    (subseq input (+ last-right-bracket 2)))
            (values input nil))
        (values "-" (subseq input 2)))))

(defun parse-syslog-message (msg)
  (multiple-value-bind
        (results structured-data-start)
      (split-sequence:split-sequence #\Space msg
                                     :count 6)
    (destructuring-bind (pri-version timestamp hostname app-name procid msgid)
        results
      (multiple-value-bind (structured-data msg)
          (split-structured-data-and-msg (subseq msg structured-data-start))
        (with-syslog-nullables (hostname app-name procid msgid structured-data)
          (declare (ignore structured-data))
          (multiple-value-bind (facility severity)
              (parse-pri-version pri-version)
            (make-instance 'syslog-message
                           :facility facility
                           :severity severity
                           :hostname hostname
                           :app-name app-name
                           :procid procid
                           :msgid msgid
                           :timestamp (parse-timestamp timestamp)
                           :msg msg)))))))

(defclass syslog-component (sxcl::xmpp-component)
  ((muc-source-map
    :initform (make-hash-table :test 'equal)
    :accessor muc-source-map)))

(defun get-muc-source (comp destination desired-nickname)
  "Returns a source full JID for sending messages to the MUC DESTINATION under the DESIRED-NICKNAME. If we don't have a JID that can send as such, makes one and joins it first."
  (sxcl::with-component-data-lock (comp)
    (let* ((ht-key (format nil "~A-~A" desired-nickname destination))
           (existing-jid (gethash ht-key (muc-source-map comp))))
      (if existing-jid
          existing-jid
          (let ((new-jid (format nil "~A@~A/syslog"
                                 (string-downcase (sha1:sha1-hex ht-key))
                                 (sxcl::component-name comp)))
                (to (format nil "~A/~A" destination desired-nickname)))
            ;; FIXME(eta): If the join fails, we're screwed.
            (sxcl::with-presence (comp to
                                  :from new-jid)
              (cxml:with-element "x"
                (cxml:attribute "xmlns" sxcl::+muc-ns+)))
            (setf (gethash ht-key (muc-source-map comp)) new-jid))))))

(defun send-syslog-message-to-destination (comp msg destination)
  "Sends the syslog message MSG to DESTINATION."
  (sxcl::with-component-data-lock (comp)
    (let* ((nickname (or
                      (when (and (app-name msg) (procid msg))
                        (format nil "~A[~A]" (app-name msg) (procid msg)))
                      (app-name msg) (hostname msg)
                      (return-from send-syslog-message-to-destination)))
           (muc-source (get-muc-source comp destination nickname)))
      (sxcl::with-message (comp destination
                           :from muc-source
                           :type "groupchat")
        (cxml:with-element "body"
          (cxml:text (format nil "~A ~A"
                             (pri-keyword-to-emoji (severity msg))
                             (string-right-trim '(#\Space #\Tab #\Newline) (msg msg)))))
        (when (timestamp msg)
          (cxml:with-element "delay"
            (cxml:attribute "xmlns" sxcl::+delivery-delay-ns+)
            (cxml:attribute "from" (sxcl::component-name comp))
            (cxml:attribute "stamp" (local-time:format-timestring nil (timestamp msg)))))
        (cxml:with-element "syslog-data"
          (cxml:attribute "xmlns" "https://theta.eu.org/syslog-xmpp")
          (cxml:with-element "facility"
            (cxml:text (princ-to-string (facility msg))))
          (cxml:with-element "severity"
            (cxml:text (princ-to-string (severity msg))))
          (when (hostname msg)
            (cxml:with-element "hostname"
              (cxml:text (princ-to-string (hostname msg)))))
          (when (app-name msg)
            (cxml:with-element "app-name"
              (cxml:text (princ-to-string (app-name msg)))))
          (when (msgid msg)
            (cxml:with-element "msgid"
              (cxml:text (princ-to-string (msgid msg)))))
          (when (procid msg)
            (cxml:with-element "procid"
              (cxml:text (princ-to-string (procid msg))))))))))

(defun send-syslog-message (comp msg)
  "Sends the syslog message MSG to all destinations specified by DETERMINE-MESSAGE-DESTINATIONS."
  (loop
    for dest in (determine-message-destinations msg)
    do (send-syslog-message-to-destination comp msg dest)))

(defun start-server (component host port)
  (let ((socket (usocket:socket-connect nil nil
                                        :protocol :datagram
                                        :element-type '(unsigned-byte 8)
                                        :local-host host
                                        :local-port port))
        (buffer (make-array 65507
                            :element-type '(unsigned-byte 8))))
    (unwind-protect
         (loop
           (handler-case
               (multiple-value-bind (buf length remote-host remote-port)
                   (usocket:socket-receive socket buffer nil)
                 (declare (ignore buf))
                 (let* ((message (babel:octets-to-string (subseq buffer 0 length)
                                                         :encoding :utf-8))
                        (smsg (parse-syslog-message message)))
                   ;; Let's not have message loops
                   ;;(format *debug-io* "~&[+] msg from ~A ~A: ~A~&" remote-host remote-port smsg)
                   (send-syslog-message component smsg)))
             (sb-sys:interactive-interrupt ()
               (format *debug-io* "~&[*] SIGTERM received, exiting.~%")
               (return-from start-server))
             (usocket:unknown-error (e)
               (format *debug-io* "~&[*] USOCKET unknown error, dying: ~A~&" e)
               (return-from start-server))
             (error (e)
               (format *error-output* "~&[!] Error (~A): ~A" (type-of e) e))))
      (usocket:socket-close socket))))

(defun start (listen-host listen-port server port shared-secret component-name)
  (let* ((xc (sxcl::make-component server port shared-secret component-name))
         (syslog-c (change-class xc 'syslog-component)))
    (start-server syslog-c listen-host listen-port)))

(defun main ()
  (when (< (length sb-ext:*posix-argv*) 2)
    (format *error-output* "fatal: a path to the config must be provided~%")
    (format *error-output* "usage: ~A CONFIG_PATH~%" (elt sb-ext:*posix-argv* 0))
    (sb-ext:exit :code 2 :abort t))
  (let ((config-path (elt sb-ext:*posix-argv* 1)))
    (format t "[+] syslog-xmpp, the cursed syslog to XMPP exporter~%")
    (format t "[+] Going to (LOAD ~A). We hope you do something useful and don't just use this to do arbitrary code exec.~%" config-path)
    (load config-path)
    (format t "[+] Calling (USER-MAIN), which we hope is defined...~%")
    (user-main)))
